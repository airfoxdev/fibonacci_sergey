package com.example.fibonaccinumber;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final String INPUT_EXTRA = "input";
    private static final String RESULT_EXTRA = "result";
    private static final String IS_RESULT_HINT_SHOWN = "is_hint_shown";

    private TextView result;
    private TextView hint;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = (TextView) findViewById(R.id.result);
        hint = (TextView) findViewById(R.id.hint);
        input = (EditText) findViewById(R.id.input);
        Button start = (Button) findViewById(R.id.start);

        if (savedInstanceState != null) {
            input.setText(savedInstanceState.getString(INPUT_EXTRA));
            result.setText(savedInstanceState.getString(RESULT_EXTRA));
            hint.setVisibility(savedInstanceState.getBoolean(IS_RESULT_HINT_SHOWN) ? View.VISIBLE : View.GONE);
        }

        SharedPreferences prefs = getSharedPreferences(Const.RESULT_PREF, MODE_PRIVATE);

        if (prefs.contains(Const.RESULT_EXTRA)) {
            updateUI(prefs.getString(Const.CALC_RESULT_EXTRA, "0"));
        }

        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    startCalculation();
                    return true;
                }
                return false;
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCalculation();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(INPUT_EXTRA, input.getText().toString());
        outState.putString(RESULT_EXTRA, result.getText().toString());
        outState.putBoolean(IS_RESULT_HINT_SHOWN, hint.getVisibility() == View.VISIBLE);
        super.onSaveInstanceState(outState);
    }


    private void startCalculation() {
        int inputNumber = Integer.parseInt(input.getText().toString());
        if (inputNumber < 0 || inputNumber > 1000) {
            Toast.makeText(this, R.string.number_error, Toast.LENGTH_SHORT).show();
            return;
        }

        result.setText("");

        FibonacciService.startFibonacciNumberCalculation(this, inputNumber, new FibonacciResultReceiver(null));
        hint.setVisibility(View.VISIBLE);
    }

    private void updateUI(String fibonacciNumber) {
        result.setText(fibonacciNumber);
        hint.setVisibility(View.GONE);
        consumeResult();
    }

    private void consumeResult() {
        SharedPreferences prefs = getSharedPreferences(Const.RESULT_PREF, MODE_PRIVATE);
        prefs.edit().remove(Const.CALC_RESULT_EXTRA).apply();
    }

    class UpdateUI implements Runnable {

        String fibonacciNumber;

        UpdateUI(String fibonacciNumber) {
            this.fibonacciNumber = fibonacciNumber;
        }

        public void run() {
            updateUI(fibonacciNumber);
        }
    }

    class FibonacciResultReceiver extends ResultReceiver {
        FibonacciResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            runOnUiThread(new UpdateUI(resultData.getString(Const.CALC_RESULT_EXTRA)));
        }
    }
}
