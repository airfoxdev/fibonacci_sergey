package com.example.fibonaccinumber;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import java.math.BigDecimal;

import static com.example.fibonaccinumber.Const.CALC_RESULT_EXTRA;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class FibonacciService extends IntentService {
    private static final String EXTRA_ITERATION_COUNT = "com.example.fibonaccinumber.extra.EXTRA_ITERATION_COUNT";
    private static final String EXTRA_RESULT_RECIEVER = "com.example.fibonaccinumber.extra.EXTRA_RESULT_RECIEVER";

    public FibonacciService() {
        super("FibonacciService");
    }

    public static void startFibonacciNumberCalculation(Context context, int count, ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, FibonacciService.class);
        intent.putExtra(EXTRA_ITERATION_COUNT, count);
        intent.putExtra(EXTRA_RESULT_RECIEVER, resultReceiver);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            int count = intent.getIntExtra(EXTRA_ITERATION_COUNT, 0);
            ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_RESULT_RECIEVER);

            BigDecimal result = fibonacci(count);

            Bundle bundle = new Bundle();
            bundle.putString(CALC_RESULT_EXTRA, result.toPlainString());
            resultReceiver.send(0, bundle);
            getSharedPreferences(Const.RESULT_PREF, MODE_PRIVATE).edit().putString(Const.CALC_RESULT_EXTRA, result.toPlainString()).apply();
        }
    }

    public static BigDecimal fibonacci(int number) {
        if (number == 1 || number == 2) {
            return new BigDecimal(1);
        }
        BigDecimal fibo1 = new BigDecimal(1);
        BigDecimal fibo2 = new BigDecimal(1);
        BigDecimal fibonacci = new BigDecimal(1);
        for (int i = 3; i <= number; i++) {
            fibonacci = fibo1.add(fibo2);
            fibo1 = fibo2;
            fibo2 = fibonacci;
        }
        return fibonacci;
    }

}
