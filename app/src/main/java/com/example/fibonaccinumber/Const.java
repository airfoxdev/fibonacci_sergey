package com.example.fibonaccinumber;


class Const {
    static final String CALC_RESULT_EXTRA = "com.example.fibonaccinumber.extra.CALC_RESULT_EXTRA";
    static final String RESULT_PREF = "com.example.fibonaccinumber.pref.PREF_NAME";
    static final String RESULT_EXTRA = "com.example.fibonaccinumber.pref.PREF_EXTRA";
}
